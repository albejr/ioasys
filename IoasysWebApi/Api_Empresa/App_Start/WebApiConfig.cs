﻿using Api_Empresa.Filters;
using System.Globalization;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Api_Empresa
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API Filters
            //config.Filters.Add(new JwtAuthentication());

            // Web API routes
            config.MapHttpAttributeRoutes();

            #region MediaTypeFormatter

            //config.Formatters.Clear();

            //XML
            //config.Formatters.Add(new XmlMediaTypeFormatter());
            //config.Formatters.XmlFormatter.AddUriPathExtensionMapping("xml", "text/xml");
            //config.Formatters.XmlFormatter.Indent = true;

            //JSON
            //config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.JsonFormatter.AddUriPathExtensionMapping("json", "application/json");
            config.Formatters.JsonFormatter.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings()
            {
                Culture = CultureInfo.GetCultureInfo("pt-BR"),
                DateFormatString = "dd/MM/yyyy",
                Formatting = Newtonsoft.Json.Formatting.Indented
            };

            #endregion
        }
    }
}
