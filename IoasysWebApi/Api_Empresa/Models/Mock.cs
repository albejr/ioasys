﻿using System.Collections.Generic;

namespace Api_Empresa.Models
{
    public static class Mock
    {
        public static List<UsuarioModel> LstUsuario()
        {
            return new List<UsuarioModel>{
                new UsuarioModel{
                    ID = 1,
                    Email="jr.albertino@outlook.com",
                    Nome = "Albertino Júnior",
                    Perfis = new string []{ }
                }
            };
        }

        public static List<EmpresaModel> LstEmpresa()
        {
            return new List<EmpresaModel>{
                new EmpresaModel{
                    ID = 1,
                    Nome = "Empresa A",
                    Tipo = EnumTipo.Matriz
                },
                new EmpresaModel{
                    ID = 2,
                    Nome = "Empresa B",
                    Tipo = EnumTipo.Filial
                },
                new EmpresaModel{
                    ID = 3,
                    Nome = "Empresa C",
                    Tipo = EnumTipo.Filial
                }
            };
        }
    }
}