﻿namespace Api_Empresa.Models
{
    public class EmpresaModel
    {
        public int ID { get; set; }

        public string Nome { get; set; }

        public EnumTipo Tipo { get; set; }
    }

    public enum EnumTipo : int
    {
        Matriz = 1,
        Filial = 2
    }
}