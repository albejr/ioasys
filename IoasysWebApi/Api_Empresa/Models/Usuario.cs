﻿using System.Collections.Generic;

namespace Api_Empresa.Models
{
    public class UsuarioModel
    {
        public int ID { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public IEnumerable<string> Perfis { get; set; }
    }
}