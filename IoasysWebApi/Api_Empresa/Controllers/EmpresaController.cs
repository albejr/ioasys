﻿using Api_Empresa.Filters;
using Api_Empresa.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebApi.OutputCache.V2;

namespace Api_Empresa.Controllers
{
    [CacheOutput(ClientTimeSpan = 90, ServerTimeSpan = 90)]
    [RoutePrefix("enterprises")]
    public class EmpresaController : ApiController
    {
        private readonly List<EmpresaModel> lstEmpresa = new List<EmpresaModel>();

        public EmpresaController() { }

        public EmpresaController(List<EmpresaModel> empresas)
        {
            this.lstEmpresa = empresas;
        }


        [JwtAuthentication]
        [HttpGet]
        [Route("")]
        public IEnumerable<EmpresaModel> Get()
        {
            return Mock.LstEmpresa();
        }

        [JwtAuthentication]
        [HttpGet]
        [Route("get/{id:int}")]
        public EmpresaModel Get(int id)
        {
            return Mock.LstEmpresa()
                .SingleOrDefault(c => c.ID == id);
        }

        [JwtAuthentication]
        [HttpGet]
        [Route("getByType/{tipo}/{nome}")]
        public IEnumerable<EmpresaModel> Get(int tipo, string nome)
        {
            return Mock.LstEmpresa()
                .Where(c => c.Tipo == (EnumTipo)tipo
                    && c.Nome.Contains(nome))
                .ToList();
        }
    }
}
