﻿using Api_Empresa.Models;
using Api_Empresa.Security;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;

namespace Api_Empresa.Controllers
{
    [RoutePrefix("account")]
    public class AcessoController : ApiController
    {
        [HttpPost]
        [AllowAnonymous]
        [Route("authenticate")]
        public IHttpActionResult Authenticate(HttpRequestMessage request, UsuarioModel user)
        {
            return Mock.LstUsuario().Any(c => c.ID == user.ID && c.Email == user.Email) ?
                ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, JwtAuthManager.GenerateJWTToken(user.Email))) :
                ResponseMessage(Request.CreateResponse(HttpStatusCode.Unauthorized, "Usuário não Identificado!"));
        }
    }
}
