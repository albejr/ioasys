using Api_Empresa.App_Start;
using System.Web.Http;
using System.Web.Routing;

namespace Api_Empresa
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
