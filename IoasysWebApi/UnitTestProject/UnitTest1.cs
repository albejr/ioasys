﻿using Api_Empresa.Controllers;
using Api_Empresa.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod()
        {
            var lst = Mock.LstEmpresa();

            var controller = new EmpresaController(lst);

            var result = controller.Get((int)EnumTipo.Filial, "C") as List<EmpresaModel>;

            Assert.AreNotEqual(lst.Count, result.Count);
        }
    }
}
